from django.shortcuts import render, redirect
from tasks.forms import TaskForm
from django.contrib.auth.decorators import login_required
from tasks.models import Task


@login_required
def create_task(request):
    if request.method == "GET":
        form = TaskForm()
    else:
        form = TaskForm(request.POST)
        if form.is_valid():
            form.save()
            return redirect("list_projects")
    return render(request, "tasks/create.html", {"form": form})


@login_required
def task_list(request):
    context = {"tasks": Task.objects.filter(assignee=request.user)}
    return render(request, "tasks/list.html", context)
