from django.urls import path
from accounts.views import login_view, signout, signup_view

urlpatterns = [
    path("login/", login_view, name="login"),
    path("logout/", signout, name="logout"),
    path("signup/", signup_view, name="signup"),
]
